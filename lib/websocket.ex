defmodule Websocket do
  use WebSockex
  require Logger

  @url Application.get_env(:websocket, :url)

  def send(topic, event, payload \\ %{}) do
    WebSockex.cast(
      __MODULE__,
      {
        :send,
        {:text, """
        {
          "topic": "#{topic}",
          "event": "#{event}",
          "payload": #{Jason.encode!(payload)},
          "ref": null
        }
        """}
      }
    )
  end

  def start_link(url \\ @url, state) do
    {:ok, pid} = WebSockex.start_link(url, __MODULE__, state, name: __MODULE__)

    Heartbeat.start_link(pid)
    {:ok, pid}
  end

  def handle_connect(_conn, state) do
    Logger.info("Connected!")
    {:ok, state}
  end

  def handle_frame({type, msg}, state) do
    decoded = Jason.decode!(msg)

    if decoded["topic"] != "phoenix" do
      Logger.info("""

      \tReceived at topic #{decoded["topic"]}:
      \tEvent: #{decoded["event"]},
      \tPayload:
      #{inspect(decoded["payload"], pretty: true)}
      """)
    end

    {:ok, state}
  end

  def handle_cast({:send, {type, msg} = frame}, state) do
    Logger.info("Sending: #{msg}")
    {:reply, frame, state}
  end

  def handle_cast(:heartbeat, state) do
    {
      :reply,
      {:text, """
      {
        "topic": "phoenix",
        "event": "heartbeat",
        "payload": {},
        "ref": null
      }
      """},
      state
    }
  end

  # def handle_cast(s, state) do
  #   Logger.info(inspect(s))
  #   {:reply, s, state}
  # end

  def handle_disconnect(%{reason: {:local, reason}}, state) do
    Logger.info("Local close with reason: #{inspect(reason)}")
    {:ok, state}
  end

  def handle_disconnect(disconnect_map, state) do
    super(disconnect_map, state)
  end

  def terminate(reason, state) do
    IO.puts("Socket Terminating:\n#{inspect(reason)}\n\n#{inspect(state)}\n")
    exit(:normal)
  end
end
