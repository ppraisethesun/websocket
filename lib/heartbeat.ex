defmodule Heartbeat do
  use GenServer
  require Logger

  def start_link(pid) do
    GenServer.start_link(__MODULE__, pid)
  end

  def init(pid) do
    schedule_work()
    {:ok, pid}
  end

  def handle_info(:send, pid) do
    WebSockex.cast(
      pid,
      :heartbeat
    )

    schedule_work()
    {:noreply, pid}
  end

  defp schedule_work() do
    Process.send_after(self(), :send, 2000)
  end
end
